'use strict';

function foo () {
	this.text = "Some text from foo";
}

foo.prototype.showText1 = function() {// TODO: bind 'this' to handle error
	console.log('text outside foobar ==', this.text);
	return function fooBar () {
		console.log('text inside foobar ==', this.text);
	}
};

foo.prototype.showText2 = () => { // 'this' is undefined here
	console.log('text outside foobar ==', this.text);
	return () => {
		console.log('text inside foobar ==', this.text);
	}
}

foo.prototype.showText3 = function () {// 'this' of this function is being used by inner arrow functions
	console.log('text outside foobar ==', this.text);
	return () => {
		console.log('text inside foobar ==', this.text);
		return () => {
			console.log('text inside inside foobar ==', this.text);
		}
	}
}

// instatntiating the constructor function
var fooObj = new foo();

fooObj.showText1()();// this will throw error until you implement TODO comment
fooObj.showText2()();
fooObj.showText3()()();