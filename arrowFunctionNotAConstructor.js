'use strict';

const foo = function(){
	this.name = 'Jenny';
}

const bar = () => {
	this.name = 'Jack';
}

let fooObj = new foo();
let barObj = new foo();// cannot be instantiated with new, an error is expected here in case of arrow function
