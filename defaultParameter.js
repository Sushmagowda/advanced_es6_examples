//es5 way

function addNumbers(a, b){

	return a + b;

}

console.log("Function without default parameters "+addNumbers(5, 4));



console.log("**********************");



function addNumbers(a, b){

	b = (typeof b !== 'undefined') ?  b : 1;

	return a + b;

}
console.log(addNumbers(5));

console.log("**********************");

//Default value assignment in ES6

function addNumbers(a, b=a){

	return a + b;

}

console.log(addNumbers(5));


console.log("**********************");

function getDefaultIncrement() {
  return 1;
}

//function as a default paarmeter

function increment(number, increment = getDefaultIncrement()) {
  return number + increment;
}

console.log(increment(2));
console.log(increment(2, 3));