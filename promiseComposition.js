'use strict';

const p1 = new Promise((resolve, reject) => {
	setTimeout(() => {
		resolve('First response resolved');
	}, 1000)
});

const p2 = new Promise((resolve, reject) => {
	setTimeout(() => {
		resolve('Second response resolved');
	}, 2000)
});

const p3 = new Promise((resolve, reject) => {
	setTimeout(() => {
		resolve('Third response resolved');
	}, 3000)
});

Promise.all([p1, p2, p3]).then((res) => {
	console.log(res);// TODO: Try error condition as it fails if any of the promises is rejected
})