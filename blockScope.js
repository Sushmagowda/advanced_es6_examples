function blockScope() {
    if (true) {
        let tmpVar = 100;
        console.log("Temporary Variable when block scoped" + tmpVar); //100
    }
    let tmpVar = 200;
    console.log("Temporary Variable when block scoped" + tmpVar); //200
}

function functionScope() {
    if (true) {
        var tmpVar = 100;
    }
    tmpVar = 200;
    console.log("Temporary Variable when function scoped " + tmpVar); //200
}

functionScope();

console.log("**********************");

//const in es6

const number = 7;

number = 19; //throws error as const is immutable already assigned to 7

console.log('Number is: ' + number);

const obj = Object.freeze({a:1}); //freeze immutable object

obj.a = 2; //cannot change the key of an frozen object

console.log(obj);